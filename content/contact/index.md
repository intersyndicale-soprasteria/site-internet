---
title:              "Contact"
type:               "contact"
resImgTeaser:       "poing.jpg"
icon:               "far fa-address-card"
---

# L'intersyndicale s'organise pour vous !

L'intersyndicale CGT / Solidaires à SopraSteria s'est constituée afin de ne plus laisser la direction
s'organiser avec ses copinages internes.

Notre intersyndicale est constituée de deux syndicats qui ont montré par les actes que leurs syndiqué·e·s
sont ouverts aux enjeux sociétaux, climatiques, et qu'ils ne font pas de distinction avec le combat à mener
à SopraSteria pour une meilleure répartition des richesses que vous avez créées, pour de meilleurs conditions
de travail, mais aussi pour la réduction du temps de travail

Pour nous contacter, merci d'envoyer un mail à contact@intersyndicale-soprasteria.fr
