---
title:              "Nos tracts"
type:               "contact"
resImgTeaser:       "nuage-mots.png"
icon:               "fas fa-file-invoice"
---

# Les tracts que nous avons distribués pendant la campagne

[![votez solidaires cgt](/resources/images/tracts/votez_sol_cgt.png)](/tracts/Votez_solidaires_cgt.pdf)
[![tract du 6 novembre colomiers](/resources/images/tracts/Tract_du_6_Colomiers-0.png)](/tracts/Tract_du_6_Colomiers.pdf)
[![manuel d'autodéfense du salarié](/resources/images/tracts/Manuel_autodefense-0.png)](/tracts/manuel_autodefense.pdf)
[![tract limonest 19 octobre](/resources/images/tracts/Tract_limonest_1910-0.png)](/tracts/Tract_limonest_1910.pdf)
[![tract cgt 1](/resources/images/tracts/tract1_cgt_soprasteria.png)](/tracts/tract1_cgt_soprasteria.pdf)
[![tract cgt 2](/resources/images/tracts/tract2_cgt_soprasteria-0.png)](/tracts/tract2_cgt_soprasteria.pdf)
[![tract si 1](/resources/images/tracts/si1-0.png)](/tracts/si1.pdf)
[![tract si 2](/resources/images/tracts/si2-0.png)](/tracts/si2.pdf)
[![remnuérations](/resources/images/tracts/remun_nos_revendications-0.png)](/tracts/remun_nos_revendications.pdf)

# Professions de foi
## SSG
[![profession de foi SSG](/resources/images/tracts/PF_SSG-0.png)](/tracts/PF_SSG.pdf)
## ICS (ex I2S)
[![profession de foi SSG](/resources/images/tracts/PF_I2S-0.png)](/tracts/PF_I2S.pdf)
## SHRS
[![profession de foi SSG](/resources/images/tracts/PF_SHRS-0.png)](/tracts/PF_SHRS.pdf)
