---
weight:      110
title:       "Le projet de l’intersyndicale pour la mandature 2023-2027"
##resImgTeaser: resources/images/Logo-intersyndicale.png
description: 
tags:
  - projet
  - mandature
categories:
  - tracts
---

## Être une force représentative **pour peser sur les accords d’entreprise** de l’U.E.S
Se regrouper en intersyndicale, c’est vouloir être
présent au niveau de l’U.E.S pour négocier des
accords d’entreprise qui représentent des avancées
sociales significatives en faveur des salarié·e·s à
l’inverse du syndicat Trait-union qui seul signe un
accord temps de travail avec une demie journée de congés
supplémentaire et en contrepartie laisse le champs
libre à la direction pour empêcher les salarié·e·s de
réclamer les 2 jours supplémentaires de
fractionnement autorisés au niveau de la branche
Syntec !

## Obtenir au niveau de chaque CSE d’entreprise SSG et I2S **une majorité pour le vote d’expertise**
Pour les décisions prises par la direction en matière
d’organisation du travail, l’intersyndicale proposera
l’appel à des experts sur des sujets relatifs à la
santé et la sécurité des salarié·e·s. A la différence des
syndicats pro-direction TU, CGC, S3I qui ont encore
empêché le vote d’une expertise sur la mise en place
d’un badge biométrique sur Toulouse !

## Représenter un plus grand nombre de salarié·e·s pour imposer à la direction un **rapport de force**
Chaque année le versement des dividendes représente
35% du résultat pour culminer à 87 millions d’euros en 2023
alors que les augmentations de salaire atteignent
difficilement 4,08% bien en dessous de la moyenne des
entreprises du privé avec un niveau d’inflation supérieur à
5% cette année ! Pire, 33% des salarié·e·s soit près de 4000
salarié·e·s pour SSG ne sont pas augmenté·e·s chaque année !
L’intersyndicale veut imposer un véritable rapport de
force et une meilleure répartition des richesses, fruit du
travail des salarié·e·s.
