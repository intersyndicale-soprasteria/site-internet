# site-internet

Le contenu du site internet de l'intersyndicale SopraSteria pour les élections pro de 2023

Le site est construit par HUGO: https://gohugo.io/installation/linux/ depuis un raspberry pi, avec un autre raspbberry pi en backup sur un autre réseau pour la résilience. La version retenue est la dernière release: hugo_0.117.0_linux-arm.tar.gz 
Il n'y a pas de version «extended» d'HUGO pour l'architecture ARM, il a fallu trouver un thème qui ne nécessite pas la version «extended»
Le thème utilisé est https://themes.gohugo.io/themes/hugo-theme-w3css-basic/, la doc est disponible sur le lien.

Il suffit d'éditer un nouveau fichier pour faire une nouvelle page, un nouveau menu, un nouvel item... Il suffit de modifier le contenu d'une page pour que le contenu soit construit et poussé sur le site de test. Une fois l'opération validée le contenu du site de test est poussé sur le site de production.

Le thème inclut beaucoup de balises pour formatter le contenu (shotcodes): https://it-gro.github.io/hugo-theme-w3css-basic.github.io/pages/hugo-theme-w3css-basic/page-shortcodes/ qui peuvent tous être modifiés, mais on va pas jouer à ça :)

La syntaxe pour écrire est markdown qui permet de faire de la mise en forme très facilement, inclusions d'images, de liens, faire des listes, etc. C'est assez facile à prendre en main: https://www.markdownguide.org/basic-syntax

Pour créer ou modifier un fichier, il suffit d'utiliser l'interface de codeberg (ce site), en cliquant sur le crayon pour modifier le fichier, ou sur «Ajouter un fichier...» pour soit
  - l'écrire directement : «Nouveau fichier»
  - déposer un nouveau fichier: «Téléverser un fichier»
  - appliquer juste la différence (mais y peu de chance qu'on utilise cette fonctionnalité)

:warning: ne **pas** modifier ``hugo.toml`` à moins de savoir **exactement** ce qu'on fait

:triangular_ruler: l'emplecement des fichiers détermine leur fonction
  - dans ``data/jumbotron``: ce sont les grosses vignettes du dessus, façon carroussel
  - dans ``data/photocards``: ce sont les grosses vignettes en dessous
  - dans ``data/testimonials``: ce sont des vignettes qui défilent pratique pour y mettre les photos des camarades candidat·e·s avec un petit texte genre citation

  Une fois le fichier modifié, il faut **l'enregistrer**, le mieux c'est avec une petite explication histoire qu'on sache ce qui a été modifié dans l'historique. À chaque enregistrement le site est reconstruit et poussé sur https://avant-diff.intersyndicale-soprasteria.fr, si la modif est validée il restera à le pousser en prod. La chaîne pour faire tout ça devrait être prochainement finalisée